Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do

      # TODO: refactor this block
      devise_scope :user do
        post "/sign_in", :to => 'sessions#create'
        delete "/sign_out", :to => 'sessions#destroy'
      end

      resources :users, only: [:index, :show, :create, :update] do
        member do
          put :follow, :unfollow, :block, :unblock, :accept_request, :reject_request
          post :change_password
        end
        collection do
          get :top_users, :social_net_worth
          get :followers, :following, :pending_requests
          get :reset_password
        end
      end

      resources :notifications, only: :index

      resources :posts, except: [:new, :edit] do
        member do
          get :like, :unlike
        end
        collection do
          get :following_posts, :hashtag_posts, :top_posts, :tranding_tags
        end
        resources :comments, except: [:new, :edit]
        resources :comments_giphies, except: [:new, :edit]
        resources :reports, only: [:create]
      end
    end
  end

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount Notifications::Engine => "/notifications"
  get "hashtags/:hashtag",   to: "hashtags#show",      as: :hashtag
  get "hashtags",            to: "hashtags#index",     as: :hashtags

  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations',  }

  resources :users do
    member do
      get :following, :followers
      get "messages"
      put :block, :unblock
    end
    collection do
      get :search
      put :change_password
      post "create_message"
      get :suggested_username, :messages_users
    end
  end

  resources :relationships, only: [:create, :destroy] do
    member do
      put :accept, :reject
    end
    collection do
      post :add_to_circle
    end
  end

  resources :posts do
    member do
      put :toggle_vote
    end
  	collection do
	    get 'search'
    end
    resources :comments
    resources :reports, only: [:create]
  end

  resources :activities

  resources :giphy, only: [:index]

  resources :comment_giphies, only: [:destroy, :create]

  root 'pages#index'

  get '/home' => 'pages#home'
  get '/social_net_worth' => 'pages#social_net_worth'
  get '/user/:id' => 'pages#profile'

  get '/explore' => 'pages#explore'

  get '/load_more' => 'pages#load_more'
  get '/test' => 'pages#test'
  get '/tags/:hashtag' => 'pages#tags'
  require 'sidekiq/web'

  mount Sidekiq::Web => '/sidekiq'

  resources :conversations do
    resources :messages
    collection do
      get :unread_conversation_messages
      put :mark_read
    end
  end

  # get "*path", to: redirect('/')
end
