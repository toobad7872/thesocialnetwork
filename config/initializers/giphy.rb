APP_CONFIG = YAML.load_file("#{Rails.root}/config/config.yml")[Rails.env]

Giphy::Configuration.configure do |config|
  config.version = APP_CONFIG['version']
  config.api_key = APP_CONFIG['giphy_api_key']
end
