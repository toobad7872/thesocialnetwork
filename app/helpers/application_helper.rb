module ApplicationHelper
  def resource_name
    :user
  end

  def resource_class
     User
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def bootstrap_class_for flash_type
    case flash_type.to_sym
      when :success
        "alert-success"
      when :error
        "alert-danger"
      when :alert
        "alert-warning"
      when :notice
        "alert-info"
      else
        flash_type.to_s
    end
  end

  def user_link_with_image(user, c_user)
    if c_user.blocked_user_ids.include?(user.id)
      link_to(image_tag(user.avatar_url, class: "notifications", height: "50", width: "50", lass: "img-circle img-no-padding img-responsive"), "javascript:void(0)", style: "display: inline")
    else
      link_to(image_tag(user.avatar_url, class: "notifications", height: "50", width: "50", lass: "img-circle img-no-padding img-responsive"), "/user/#{user.username}", style: "display: inline")
    end
  end

  def user_link_with_name(user, c_user)
    if c_user.blocked_user_ids.include?(user.id)
      link_to(user.username, "javascript:void(0)", style: "display: inline")
    else
      link_to(user.username, "/user/#{user.username}", style: "display: inline")
    end
  end

end
