class ActivitiesController < ApplicationController

  before_filter :authenticate_user!
  before_action :tranding_tags
  before_action :top_posts
  before_action :suggested_friends

  def index
	  @activities = PublicActivity::Activity.where(recipient_type: 'User', recipient_id: current_user.id).where("owner_id != ?", current_user.id).order("created_at desc")
	end
end
