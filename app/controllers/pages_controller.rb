# The pages controller contains all of the code for any page inside of /pages
class PagesController < ApplicationController

  before_filter :authenticate_user!, only: [:home, :explore, :tags, :social_net_worth, :profile]
  before_action :tranding_tags, only: [:home, :explore, :tags, :social_net_worth]
  before_action :top_posts, only: [:home, :explore, :tags, :social_net_worth]
  before_action :suggested_friends, only: [:home, :explore, :tags]

  #layout :resolve_layout
  # back-end code for pages/index
  def index
    if current_user.present?
      redirect_to "/home" and return
    end
    @top_users = User.limit(10) #.order("RANDOM()")
  end

  def test
  end

  # back-end code for pages/home
  def home
    @pending_requests = Relationship.where(followed_id: current_user.id, status: 'pending')
    @activities = PublicActivity::Activity.where(recipient_type: 'User', recipient_id: current_user.id).where("owner_id != ?", current_user.id).order("created_at desc")
    following = Array.new
    for @f in current_user.following.ignore_blocked_users(current_user).filter_circle(params[:circle]) do
      following.push(@f.id)
    end
    @posts = Post.includes(:reports).where("user_id IN (?)", following)
    @newPost = Post.new
  end

  # back-end code for pages/profile
  def profile
    # grab the username from the URL as :id
    @user = User.find_by_username(params[:id])
    raise ActiveRecord::RecordNotFound unless @user
    if current_user && current_user.id != @user.id
      raise CanCan::AccessDenied if current_user.blocked?(@user.id)
    end
    @posts = Post.includes(:reports).all.where("user_id = ?", @user.id)
    @newPost = Post.new
    # @toFollow = User.all.last(5)
    # if current_user.username == params[:id]
    @following =@user.following.ignore_blocked_users(@user)
    @followers = @user.followers.ignore_blocked_users(@user)
  end

  # back-end code for pages/explore
  def explore
    @posts = Post.includes(:reports).where.not(user_id: current_user.blocked_user_ids)
    @newPost = Post.new
  end

  def load_more
    @post = Post.find(params[:id])
    @comments = @post.comments
  end

  def tags
    @newPost = Post.new
    @hashtagged = []
    @hashtag = SimpleHashtag::Hashtag.find_by_name(params[:hashtag].downcase)
    if @hashtag
      @hashtagged = @hashtag.hashtaggables.select do |post|
        if post.is_a?(Post)
          !current_user.blocked_user_ids.include?(post.user_id)
        end
      end
    end
  end

  def social_net_worth
    @my_badges = current_user.badges
    @badges = Badge.all
    @pending_requests = Relationship.where(followed_id: current_user.id, status: 'pending')
    @leaderboard = PointHistory.select("*,SUM(points) as total").group(:user_id).order("total DESC")
    @point_histories = current_user.earned_point_histories
    @activities = PublicActivity::Activity.where(recipient_type: 'User', recipient_id: current_user.id).where("owner_id != ?", current_user.id).order("created_at desc")
  end

  private
  def resolve_layout
    case action_name
    when "index"
      "landing_page"
    else
      "application"
    end
  end

end
