class Api::V1::SessionsController < Api::V1::ApiController

  before_filter :authenticate_user!, :except => [:create]
  skip_before_filter  :verify_authenticity_token
  respond_to :json

  eval(IO.read('doc/api_doc/auth/sign_in.html'), binding)
  def create
	  render json: { message: "Request Parameter not valid" }, status: 401 and return unless params[:session]
    email = params[:session][:email]
    password = params[:session][:password]
    render json: { message: "The request must contain the email and password." }, status: 401 and return unless email && password
    @user = User.where(email: email).first
    @user = User.where(email: email).first unless @user
    render json: { message: "Invalid email or password" }, status: 401 and return if @user.blank?
    render json: { message: "Invalid email or password" }, status: 401 and return if not @user.valid_password?(password)
    authentication_token = @user.reset_token
    @user = @user.as_json(only: [:id, :email, :email, :name, :address, :role, :image, :created_at]).merge!(authentication_token: authentication_token)
    render json: { user: @user, message: "Successfuly Log in" }
  end

  def destroy
    if @token.destroy
      render json: { message: "Signed Out Successfuly" }
    else
      render json: { message: "Not able to signout" }
    end
  end

  private

    def registration_params
      params.require(:session).permit(:email, :email, :password, :password_confirmation, :authentication_token)
    end
end
