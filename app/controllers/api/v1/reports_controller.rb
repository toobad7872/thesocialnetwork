class Api::V1::ReportsController < Api::V1::ApiController

  before_filter :authenticate_user!
  skip_before_filter  :verify_authenticity_token
  before_filter :set_post

  def create
    report = @post.reports.build(user_id: @current_user.id, description: params[:description])
    if report.save
      render json: { message: "Success" }, status: 201
    else
      render json: { error: report.errors.to_a.to_sentence }, status: 422
    end
  end

  private

    def set_post
      @post = Post.find(params[:post_id])
    end
end
