class Api::V1::NotificationsController < Api::V1::ApiController

	before_filter :authenticate_user!, only: [:index]
  skip_before_filter :verify_authenticity_token

	eval(IO.read('doc/api_doc/notifications/list_notifications.html'), binding)
	def index
		@activities = PublicActivity::Activity.includes(:recipient, :trackable, :owner).where(recipient_type: 'User', recipient_id: @current_user.id).where("owner_id != ?", @current_user.id).order("created_at desc")
  	render json: @activities.as_json(include: [ :recipient, :trackable, :owner ])
	end

end