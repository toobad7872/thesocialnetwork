class Api::V1::ApiController < ApplicationController

  respond_to :json

	def current_user
    @token = Token.where(authentication_token: request.headers['User-Token']).first
	  @current_user = @token.try(:user)
    @current_user
	end

  def authenticate_user!
	  render json:{error:'401 Unauthorized!'}, status: 401 and return unless current_user
	end
end
