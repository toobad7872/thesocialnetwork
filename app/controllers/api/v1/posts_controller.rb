class Api::V1::PostsController < Api::V1::ApiController

  before_filter :authenticate_user!
  skip_before_filter  :verify_authenticity_token
  before_action :set_post, only: [:index, :show, :update, :destroy, :like, :unlike]
  before_action :validate_badges, only: [:create]

  eval(IO.read('doc/api_doc/posts/index.html'), binding)
  def index
    @posts = @user.posts
    render json: @posts.as_json(include: [:user])
  end

  eval(IO.read('doc/api_doc/posts/view_post.html'), binding)
  def show
    render json: @post.as_json(include: [:user, comments: { include: [:user] } ])
  end

  eval(IO.read('doc/api_doc/posts/create.txt'), binding)
  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id
    render json: { error: "Please add a attachment" }, status: 422 and return if params[:videos].blank?
    params[:videos].each do |video|
      @post.post_uploads.build(video: video)
    end
    @post.content = @post.content + ' ' + @current_user.tagline.to_s if params[:add_tagline].present?
    @post.circle = params[:circle]
    if @post.save
      render json: @post.as_json(include: [:user])
    else
      render json: { error: @post.errors.to_a }, status: 422
    end
  end

  eval(IO.read('doc/api_doc/posts/update.html'), binding)
  def update
    authorize! :update, @post
    if @post.update(update_post_params)
      render json: @post.as_json(include: [:user])
    else
      render json: { error: @post.errors.to_a }, status: 422
    end
  end

  def destroy
    authorize! :destroy, @post
    if @post.destroy
      render json: { message: "Post success fully deleted" }
    else
      render json: {error: "Not able to delete post"}, status: 422
    end
  end

  eval(IO.read('doc/api_doc/posts/like_post.html'), binding)
  def like
    if @post.votes_for.pluck(:voter_id).include?(@current_user.try(:id))
      render json: { error: "You have already Liked this Post" }, status: 422
    else
      vote = @post.liked_by(@current_user)
      unless @current_user.posts.pluck(:id).include? @post.id
        s = ActsAsVotable::Vote.find_by( voter_id: @current_user.id, voter_type: "User", votable_id: @post.id, votable_type: "Post")
        if s.present?
          @post_owner_social_net_worth = SocialNetWorth.create(point: 1, amount: 0.25, user_id: @post.user_id, post_id: @post.id, redeem_status: false, pointable_id: s.id, pointable_type:  "ActsAsVotable::Vote")
          @social_net_worth = SocialNetWorth.create(point: 1, amount: 0.25, user_id: @current_user.id, post_id: @post.id, redeem_status: false, pointable_id: s.id, pointable_type: "ActsAsVotable::Vote")
        end
      end
      @post.votes_for.last.create_activity :create ,owner: current_user
      render json: @post.as_json(include: [:user])
    end
  end

  eval(IO.read('doc/api_doc/posts/unlike_post.html'), binding)
  def unlike
    if @post.votes_for.pluck(:voter_id).include?(@current_user.try(:id))
      vote = ActsAsVotable::Vote.find_by(voter_id: @current_user.id, voter_type: "User", votable_id: @post.id, votable_type: "Post")
      @social_net_worths = SocialNetWorth.where(point: 1, amount: 0.25, user_id: [@current_user.id, @post.user_id], post_id: @post.id, redeem_status: false, pointable_id: vote.id, pointable_type: "ActsAsVotable::Vote")
      if @social_net_worths.present?
        @social_net_worths.destroy_all
      end
      @post.unliked_by(@current_user)
      render json: @post.as_json(include: [:user])
    else
      render json: { error: "You have not Liked this Post yet" }, status: 422
    end
  end

  def tranding_tags
    @hashtags = SimpleHashtag::Hashtagging.joins(:hashtag).select("simple_hashtag_hashtags.*, COUNT(simple_hashtag_hashtaggings.hashtag_id) As total_tags").where('created_at > ?', 24.hours.ago).group(:hashtag_id).order("total_tags DESC").limit(5)
    render json: @hashtags
  end

  def top_posts
    comment_posts = Post.joins(:comments).where.not(user_id: @current_user.blocked_user_ids).where("comments.created_at > ?", 1.days.ago).group(:post_id).count
    votes_posts = Post.joins(:votes_for).where.not(user_id: @current_user.blocked_user_ids).where("votes.created_at > ?", 1.days.ago).group(:votable_id).count
    posts = {}
    (comment_posts.keys | votes_posts.keys).each do |key|
      posts[key] = comment_posts[key].to_i + votes_posts[key].to_i
    end
    posts = posts.sort_by {|k,v| v}.reverse[0..10].to_h
    @top_posts = Post.where(id: posts.keys)
    render json: @top_posts.as_json(include: [:user])
  end

  def hashtag_posts
    @posts = []
    @hashtag = SimpleHashtag::Hashtag.where(name: params[:hashtag].downcase).last
    render json: { error: "Hashtag not found" }, status: 404 and return unless @hashtag
    @posts = @hashtag.hashtaggables.select do |post|
      if post.is_a?(Post)
        !@current_user.blocked_user_ids.include?(post.user_id)
      end
    end
    render json: @posts
  end

  def following_posts
    user_ids = @current_user.following.ignore_blocked_users(@current_user).pluck(:id)
    @posts = Post.where(user_id: user_ids)
    render json: @posts
  end

  private

    def post_params
      params.require(:post).permit(:user_id, :content)
    end

    def update_post_params
      params.require(:post).permit(:content)
    end

    def set_post
      @post = Post.find(params[:id])
    end

    def validate_badges
      message = nil
      videos_count = params[:videos].present? ? params[:videos].size : 0
      videos_size = (params[:videos].present? && params[:videos].size > 1) ? params[:videos].map(&:size).inject(:+)/1024/1024 : 0
      case current_user.badge.try(:name)
      when 'Beginner Level'
        message = (videos_count <= 3 && videos_size <= 2) ? nil : 'You can upload max 3 files and 2 MB'
      when 'Go Getter Level'
        message = (videos_count <= 8 && videos_size <= 4) ? nil : 'You can upload max 8 files and 4 MB'
      when 'Boss Level'
        message = (videos_count <= 12 && videos_size <= 6) ? nil : 'You can upload max 12 files and 6 MB'
      when 'Star Level'
        message = (videos_count <= 15 && videos_size <= 8) ? nil : 'You can upload max 15 files and 8 MB'
      when 'Legend Level'
        message = (videos_count <= 30 && videos_size <= 10) ? nil : 'You can upload max 30 files and 10 MB'
      else
        message = (videos_count == 1 && videos_size <= 1) ? nil : 'You can upload only 1 file and 1 MB'
      end
      render json: { error: message }, status: 422 and return if message
    end
end