class Api::V1::CommentsGiphiesController < Api::V1::ApiController

  before_filter :authenticate_user!
  skip_before_filter :verify_authenticity_token
  before_action :set_post
  respond_to :json

  eval(IO.read('doc/api_doc/posts/all_comment.html'), binding)
  def index
    @comment_giphies = @post.comment_giphies.includes(:user, :post)
    render json: @comment_giphies.as_json(include: [:user, :post])
  end

  eval(IO.read('doc/api_doc/posts/post_comment.html'), binding)
  def create
    @comment_giphy = @post.comment_giphies.build(comment_giphies_params)
    @comment_giphy.user_id = @current_user.id
    if @comment_giphy.save
      render json: @comment_giphy.as_json(include: [:user, :post])
    else
      render json: { error: @comment_giphy.errors.to_a }, status: 422
    end
  end

  eval(IO.read('doc/api_doc/posts/update_comment.html'), binding)
  def update
    @comment_giphy = @post.comment_giphies.find(params[:id])
    authorize! :update, @comment_giphy
    if @comment_giphy.update(comment_giphies_params)
      render json: @comment_giphy.as_json(include: [:user, :post])
    else
      render json: { error: @comment_giphy.errors.to_a }, status: 422
    end
  end

  eval(IO.read('doc/api_doc/posts/delete_comment.html'), binding)
  def destroy
    @comment_giphy = @post.comment_giphies.find(params[:id])
    authorize! :destroy, @comment_giphy
    if @comment_giphy.destroy
      render json: nil
    else
      render json: { error: @comment_giphy.errors.to_a }, status: 422
    end
  end

  private

  def comment_giphies_params
    params.require(:comment_giphy).permit(:url, :user_id)
  end

  def set_post
    @post = Post.find(params[:post_id])
  end
end