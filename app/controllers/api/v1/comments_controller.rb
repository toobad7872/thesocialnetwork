class Api::V1::CommentsController < Api::V1::ApiController

  before_filter :authenticate_user!
  skip_before_filter  :verify_authenticity_token
  before_action :set_post
  respond_to :json

  eval(IO.read('doc/api_doc/posts/all_comment.html'), binding)
  def index
    @comments = @post.comments.includes(:user, :post)
    render json: @comments.as_json(include: [:user, :post])
  end

  eval(IO.read('doc/api_doc/posts/post_comment.html'), binding)
  def create
	  @comment = @post.comments.build(comment_params)
	  @comment.user_id = @current_user.id
	  if @comment.save
	    render json: @comment.as_json(include: [:user, :post])
	  else
	    render json: { error: @comment.errors.to_a }, status: 422
	  end
  end

  eval(IO.read('doc/api_doc/posts/update_comment.html'), binding)
  def update
    @comment = @post.comments.find(params[:id])
    authorize! :update, @comment
    if @comment.update(comment_params)
    	render json: @comment.as_json(include: [:user, :post])
    else
      render json: { error: @comment.errors.to_a }, status: 422
    end
  end

  eval(IO.read('doc/api_doc/posts/delete_comment.html'), binding)
  def destroy
    @comment = @post.comments.find(params[:id])
    authorize! :destroy, @comment
    if @comment.destroy
  	  render json: nil
    else
      render json: { error: @comment.errors.to_a }, status: 422
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :user_id)
  end

  def set_post
    @post = Post.find(params[:post_id])
  end
end