class Api::V1::UsersController < Api::V1::ApiController

  before_filter :authenticate_user!, except: [ :create, :reset_password ]
  skip_before_filter :verify_authenticity_token
  before_filter :set_user, only: [:follow, :unfollow, :block, :unblock, :accept_request, :reject_request, :show, :update ]

  eval(IO.read('doc/api_doc/users/serach_users.html'), binding)
  def index
    if params[:search].present?
      @users = User.search(params[:search])
      render json: @users
    else
      render json: { error: 'Please enter search keyword' }, status: 422
    end
  end

  def show
    render json: @user
  end

  eval(IO.read('doc/api_doc/auth/sign_up.html'), binding)
  def create
    @user = User.new(user_params)
    if @user.save
      render json: @user, status: 201
    else
      render json: { error: @user.errors }, status: 422
    end
  end

  eval(IO.read('doc/api_doc/auth/edit_profile.txt'), binding)
  def update
    if @current_user.update(user_params)
      render json: @current_user
    else
      render json: { error: @current_user.errors }, status: 422
    end
  end

  # TODO: Remaning actions
  eval(IO.read('doc/api_doc/auth/reset_password.html'), binding)
  def reset_password
    render json: { error: "Please privide email" }, status: 422 and return if params[:email].nil?
    @user = User.where(email: params[:email]).first
    render json: { error: "User not found" }, status: 404 and return unless @user
    @user.send_reset_password_instructions
    render json: { message: 'New Password instructions Sent To Email' }
  end

  eval(IO.read('doc/api_doc/auth/change_password.html'), binding)
  def change_password
    if @current_user.update(user_params)
      render json: { message: "Your password change successfully" }
    else
      render json: { error: @current_user.errors }, status: 422
    end
  end

  eval(IO.read('doc/api_doc/users/top_users.html'), binding)
  def top_users
  	@top_users = User.limit(10).order("RANDOM()")
  	render json: @top_users
  end

  eval(IO.read('doc/api_doc/users/social_net_worth.html'), binding)
  def social_net_worth
    @my_badges = @current_user.badges
    @badges = Badge.all
    @leaderboard = PointHistory.select("*,SUM(points) as total").group(:user_id).order("total DESC")
    @point_histories = @current_user.earned_point_histories
    render json: { all_badges: @badges, my_badges: @my_badges, leaderboard: @leaderboard, point_histories: @point_histories }
  end

  def follow
    render json: { error: "You are already following the user" }, status: 422 and return if @current_user.following?(@user)
    if @current_user.follow(@user)
      render json: { message: "Succesfully able to follow the user" }
    else
      render json: { error: "You are already following the user" }, status: 422
    end
  end

  def unfollow
    render json: { error: "You are already not following the user" }, status: 422 and return unless @current_user.following?(@user)
    if Relationship.where(followed_id: @user.id, follower_id: @current_user.id).last.destroy
      render json: { message: "Succesfully able to unfollow the user" }
    else
      render json: { error: "You are already not following the user" }, status: 422
    end
  end

  def accept_request
    relationship = Relationship.where(followed_id: @user.id, follower_id: @current_user.id, status: 'pending').last
    if relationship && relationship.update(status: 'accepted')
      Notification.create(notify_type: 'follow', actor: relationship.follower, user: relationship.followed)
      render json: { message: 'Request accepted' }
    else
      render json: { error: 'Request not found' }, status: 422
    end
  end

  def reject_request
    relationship = Relationship.where(followed_id: @user.id, follower_id: @current_user.id, status: 'pending').last
    if relationship && relationship.destroy
      render json: { message: 'Request rejected' }
    else
      render json: { error: 'Request not found' }, status: 422
    end
  end

  def pending_requests
    pending_following = @current_user.pending_following
    render json: pending_following
  end

  def following
    @following = @current_user.following
    render json: @following
  end

  def followers
    @followers = @current_user.followers
    render json: @followers
  end

  def block
    if @user.blocked_user_ids.include?(@current_user.id)
      render json: { error: 'Already blocked the user' }, status: 422
    else
      @user.blocked_user_ids.push(@current_user.id)
      @user.blocked_user_ids = @user.blocked_user_ids.uniq.compact
      @user.save
      render json: { message: 'You blocked the user' }
    end
  end

  def unblock
    if @user.blocked_user_ids.include?(@current_user.id)
      @user.blocked_user_ids.pop(@current_user.id)
      @user.blocked_user_ids = @user.blocked_user_ids.uniq.compact
      @user.save
      render json: { message: 'You unblocked the user' }
    else
      render json: { error: 'Already unblocked the user' }, status: 422
    end
  end

  private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:username, :email, :password, :password_confirmation, :avatar, :avatar_cache, :timezone, :dob, :name, :gender)
    end
end