class CommentsController < ApplicationController

  before_action :set_post

  def create
    @comment = @post.comments.create(user_id: current_user.id, content: params[:content])
  end

  def update
    @comment = @post.comments.find(params[:id])
    authorize! :update, @comment
    if @comment.update(comment_params)
      render json: {message: "Comment successfully updated", content: @comment.content}
    else
      render json: {errors: "Not able to update post"}, status: 422
    end
  end

  def destroy
    @comment = @post.comments.find(params[:id])
    @comment.destroy
    render json: {comment_count: @post.comments.count}
  end

  private

  def comment_params
    params.require(:comment).permit(:content)
  end

  def set_post
    @post = Post.find(params[:post_id])
  end
end
