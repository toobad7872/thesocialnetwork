class ConversationsController < ApplicationController
  before_filter :authenticate_user!

  layout false

  def create
    @conversation_exist = false
    params[:sender_id] = current_user.id if params[:add_current_user].present?
    if Conversation.between(params[:sender_id],params[:recipient_id]).present?
      @conversation_exist = true
      @conversation = Conversation.between(params[:sender_id],params[:recipient_id]).first
    else
      @conversation = Conversation.create!(conversation_params)
    end
    @message = Message.new
    respond_to do |format|
      format.js
      format.json { render json: { conversation_id: @conversation.id } }
    end
  end

  def show
    @conversation = Conversation.find(params[:id])
    @reciever = interlocutor(@conversation)
    @messages = @conversation.messages
    @message = Message.new
  end

  def unread_conversation_messages
    conversation_ids = Conversation.where('recipient_id = ? OR sender_id = ?', current_user.id, current_user.id).pluck(:id)
    unread_count = Message.where(conversation_id: conversation_ids, read: false).where.not(user_id: current_user.id).count
    render json: { unread_count: unread_count }
  end

  def mark_read
    if params[:conversation_id].to_s == '0'
      conversation_ids = Conversation.where('recipient_id = ? OR sender_id = ?', current_user.id, current_user.id).pluck(:id)
    else
      conversation_ids = params[:conversation_id]
    end
    Message.where(conversation_id: conversation_ids, read: false).where.not(user_id: current_user.id).update_all(read: true)
    ids = Conversation.where('recipient_id = ? OR sender_id = ?', current_user.id, current_user.id).pluck(:id)
    unread_count = Message.where(conversation_id: ids, read: false).where.not(user_id: current_user.id).count
    render json: { unread_count: unread_count }
  end

  private
  def conversation_params
    params.permit(:sender_id, :recipient_id)
  end

  def interlocutor(conversation)
    current_user == conversation.recipient ? conversation.sender : conversation.recipient
  end
end
