class GiphyController < ApplicationController
  def index
    giphies = Giphy.search(params[:term])
    giphies_images = giphies.map {|giphy| giphy.fixed_height_image.url.to_s }
    render json: giphies_images
  end
end