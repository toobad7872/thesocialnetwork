class MessagesController < ApplicationController
  before_filter :authenticate_user!

  def create
    @conversation = Conversation.find(params[:conversation_id])
    @message = @conversation.messages.build(message_params)
    @message.user_id = current_user.id
    @message.save!

    @path = conversation_path(@conversation)
    PrivatePub.publish_to "/messages/#{current_user.id}", :chat_message => "Hello, world!"
    PrivatePub.publish_to "/messages/#{current_user.id == @conversation.sender_id ? @conversation.recipient_id : @conversation.sender_id}", :chat_message => "Hello, world!"
  end

  def destroy
    @conversation = Conversation.find(params[:conversation_id])
    if params[:id].present? && params[:id] == 'all'
      @conversation.destroy
    else
      @message = @conversation.messages.find(params[:id])
      @message.destroy
    end
    PrivatePub.publish_to "/messages/#{current_user.id}", :chat_message => "Hello, world!"
    PrivatePub.publish_to "/messages/#{current_user.id == @conversation.sender_id ? @conversation.recipient_id : @conversation.sender_id}", :chat_message => "Hello, world!"
  end

  private

  def message_params
    params.require(:message).permit(:body, :attachment)
  end
end
