class ReportsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :set_post

  def create
    report = @post.reports.build(user_id: current_user.id, description: params[:description])
    if report.save
      render json: {message: "Success"}, status: 201
    else
      render json: {errors: report.errors.to_a.to_sentence}, status: 422
    end
  end

  def show
  end

  private

  def set_post
    @post = Post.find(params[:post_id])
  end
end
