class CommentGiphiesController < ApplicationController

  def create
    @post = Post.find(params[:post_id])
    @comment_giphy = @post.comment_giphies.build(url: params[:url], user_id: current_user.id)
    @comment_giphy.save
  end

  def destroy
    comment_giphy = CommentGiphy.where(id: params[:id]).last
    render json: {error: 'Not able to find the giphy'}, status: 404 and return unless comment_giphy
    post = comment_giphy.post
    if comment_giphy.destroy
      render json: {comment_giphies_count: post.comment_giphies.count}, status: :ok
    else
      render json: {error: 'Not able to delete the giphy'}, status: 422
    end
  end

end