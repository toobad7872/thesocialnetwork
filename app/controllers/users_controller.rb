class UsersController < ApplicationController

  before_filter :authenticate_user!, only: [:change_password, :block, :unblock, :search, :messages_users]
  before_action :set_user, only: [:show, :edit, :update, :destroy, :block, :unblock]
  before_action :set_s3_direct_post, only: [:new, :edit, :create, :update]

  def search
    @results = []
    if params[:search].present?
      # @results = Searchkick.search(params[:search], index_name: [User, Post]).to_a
      @users = User.where("name LIKE ? OR username LIKE ? OR email LIKE ? OR bio LIKE ?", "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%", "%#{params[:search]}%")
      @posts = Post.where("content LIKE ?", "%#{params[:search]}%")
      @results = (@users + @posts).compact.flatten
      @results = @results.group_by{|result| result.is_a?(User) ? "users" : "posts" }
    end
  end

  def new
		@user = User.new
	end

	def index
  	@user = User.all
  end

  def change_password
    if current_user.valid_password?(params[:user][:current_password])
      if current_user.update(change_password_params)
        sign_in(current_user, :bypass => true)
        redirect_to edit_user_registration_path, flash: {success: "Your password updated successfully"}
      else
        redirect_to :back, flash: {error: current_user.errors.to_a}
      end
    elsif
      redirect_to :back, flash: {error: "You Current Password is not matched"}
    end
  end

  def messages
    @conversations = Conversation.involving(current_user)
    if params[:conversation_id].present?
      @conversation = @conversations.find_by(:id=>params[:conversation_id])
    else
      @conversation = @conversations.first
    end
    @message = Message.new
  end

  def create_message
    @conversation = Conversation.find(params[:conversation_id])
    @message = @conversation.messages.build(message_params)
    @message.user_id = current_user.id
    @message.save!
    @path = conversation_path(@conversation)
    p "/messages/#{current_user.id}"
    p "/messages/#{current_user.id == @conversation.sender_id ? @conversation.recipient_id : @conversation.sender_id}"
    PrivatePub.publish_to "/messages/#{current_user.id}", :chat_message => "Hello, world!"
    PrivatePub.publish_to "/messages/#{current_user.id == @conversation.sender_id ? @conversation.recipient_id : @conversation.sender_id}", :chat_message => "Hello, world!"
  end

  def block
    @user.blocked_user_ids.push(current_user.id)
    @user.blocked_user_ids = @user.blocked_user_ids.uniq.compact
    @user.save
    redirect_to :back
  end

  def unblock
    @user.blocked_user_ids.pop(current_user.id)
    @user.blocked_user_ids = @user.blocked_user_ids.uniq.compact
    @user.save
    redirect_to :back
  end

  def suggested_username
    username = params[:username].downcase
    user = User.where("lower(username) = :value", { value: username }).first
    unless user
      render json: { available: true }
    else
      usernames = 10.times.map { "#{username}#{rand(100)}" }
      existing_usernames = User.where(username: usernames).pluck(:username)
      usernames = usernames - existing_usernames
      render json: { available: false, usernames: usernames[0..2] }
    end
  end

  def messages_users
    all_users = User.ignore_blocked_users(current_user).where.not(id: current_user.id).where(direct_message: true)
    friends = current_user.friends
    @users = (all_users + friends).uniq
    render json: @users
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def set_s3_direct_post
    	@s3_direct_post = S3_BUCKET.presigned_post(key: "uploads/#{SecureRandom.uuid}/${filename}", success_action_status: '201', acl: 'public-read')
  	end

    def change_password_params
      params.require(:user).permit(:password, :password_confirmation)
    end

    def message_params
      params.require(:message).permit(:body, :attachment, :gif)
    end
end