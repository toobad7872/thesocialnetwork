class ApplicationController < ActionController::Base
  include PublicActivity::StoreController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception, unless: :api_action?
    # call the configured params
  before_action :configure_permitted_parameters, if: :devise_controller?

  before_action :social_net_worths, unless: :api_action?

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { render json: { errors: exception.message }, status: :forbidden }
      format.html { redirect_to root_url, notice: exception.message }
      format.js   { head :forbidden, content_type: 'text/html' }
    end
  end

  rescue_from ActionController::RoutingError, ActiveRecord::RecordNotFound, with: :render_404

  def check_for_active
    user = User.unscoped.where(email: params[:user][:email]).last
    if user && user.disable?
      redirect_to root_url, notice: "Your Account is disabled"
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) do |user|
      user.permit(:username, :email, :password,:password_confirmation, :remember_me, :avatar, :avatar_cache, :timezone, :dob, :name, :gender)
    end
    devise_parameter_sanitizer.permit(:sign_in) do |user|
      user.permit(:login, :username, :email, :password, :remember_me)
    end
    devise_parameter_sanitizer.permit(:account_update) do |user|
      user.permit(:username, :bio, :email, :password,:password_confirmation, :current_password, :avatar, :avatar_cache, :cover, :timezone, :dob, :name, :gender, :tagline)
    end
    # devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :email, :password,:password_confirmation, :remember_me, :avatar, :avatar_cache) }
    # devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :username, :email, :password, :remember_me) }
    # devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username,:bio, :email, :password,:password_confirmation, :current_password, :avatar, :avatar_cache, :cover) }
  end

  def after_sign_in_path_for(resource)
    if current_user.admin?
      rails_admin_path
    else
      request.env['omniauth.origin'] || stored_location_for(resource) || root_path
    end
  end

  private

    def tranding_tags
      @hashtags = SimpleHashtag::Hashtagging.joins(:hashtag).select("simple_hashtag_hashtags.*, COUNT(simple_hashtag_hashtaggings.hashtag_id) As total_tags").where('created_at > ?', 24.hours.ago).group(:hashtag_id).order("total_tags DESC").limit(5)
    end

    def top_posts
      comment_posts = Post.joins(:comments).where.not(user_id: current_user.blocked_user_ids).where("comments.created_at > ?", 1.days.ago).group(:post_id).count
      votes_posts = Post.joins(:votes_for).where.not(user_id: current_user.blocked_user_ids).where("votes.created_at > ?", 1.days.ago).group(:votable_id).count
      posts = {}
      (comment_posts.keys | votes_posts.keys).each do |key|
        posts[key] = comment_posts[key].to_i + votes_posts[key].to_i
      end
      posts = posts.sort_by {|k,v| v}.reverse[0..10].to_h
      @top_posts = Post.where(id: posts.keys)
    end

    def suggested_friends
      if current_user
        @toFollow = current_user.suggested_friends.first(3)
      else
        @toFollow = User.all.last(3)
      end
    end

    def render_404
      if api_action?
        render json: { error: 'Record Not Found' }, status: 404
      else
        render file: "#{Rails.root}/public/404.html", status: 404, layout: false
      end
    end

    def social_net_worths
      @point_histories = current_user.earned_point_histories if current_user.present?
    end

    def api_action?
      request.path.match("/api/v1").present?
    end
  # def current_user
  #   @current_user ||= current_user
  # end
  # helper_method :current_user
  # hide_action :current_user

end