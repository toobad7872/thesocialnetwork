class PostsController < ApplicationController

  include ActionView::Helpers::OutputSafetyHelper

  before_action :set_post, only: [:update, :destroy, :show, :toggle_vote]

  def new
    @post = Post.new
  end

  def show
  end

  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id # assign the post to the user who created it.
    return redirect_to :back, flash: { error: "Please add a attachment" } if params[:videos].blank?
    params[:videos].each do |video|
      @post.post_uploads.build(video: video)
    end
    @post.content = @post.content + ' ' + current_user.tagline.to_s if params[:add_tagline].present?
    @post.circle = params[:circle]
    respond_to do |f|
      if (@post.save)
        f.html { redirect_to ""}
      else
        f.html { redirect_to "", flash: { error: @post.errors.to_a } }
      end
    end
  end

  def update
    authorize! :update, @post
    if @post.update(update_post_params)
      render json: {message: "Post successfully updated", content: raw(@post.content.gsub(/#([A-z]\w+)/, "<a href='/tags/\\1'>#\\1</a>"))}
    else
      render json: {errors: "Not able to delete post"}, status: 422
    end
  end

  def destroy
    authorize! :destroy, @post
    if @post.destroy
      render json: {message: "Post success fully deleted"}
    else
      render json: {errors: "Not able to delete post"}, status: 422
    end
  end

  def toggle_vote
    if(@post.votes_for.pluck(:voter_id).include?(current_user.try(:id)))
      vote = ActsAsVotable::Vote.find_by(voter_id: current_user.id, voter_type: "User", votable_id: @post.id, votable_type: "Post")
      @social_net_worths = SocialNetWorth.where(point: 1, amount: 0.25, user_id: [current_user.id, @post.user_id], post_id: @post.id, redeem_status: false, pointable_id: vote.id, pointable_type: "ActsAsVotable::Vote")
      if @social_net_worths.present?
        @social_net_worths.destroy_all
      end
      @post.unliked_by(current_user)
    else
      vote = @post.liked_by(current_user)
      unless current_user.posts.pluck(:id).include? @post.id
        s = ActsAsVotable::Vote.find_by( voter_id: current_user.id, voter_type: "User", votable_id: @post.id, votable_type: "Post")
        if s.present?
          @post_owner_social_net_worth = SocialNetWorth.create(point: 1, amount: 0.25, user_id: @post.user_id, post_id: @post.id, redeem_status: false, pointable_id: s.id, pointable_type:  "ActsAsVotable::Vote")
          @social_net_worth = SocialNetWorth.create(point: 1, amount: 0.25, user_id: current_user.id, post_id: @post.id, redeem_status: false, pointable_id: s.id, pointable_type: "ActsAsVotable::Vote")
        end
      end
      @post.votes_for.last.create_activity :create ,owner: current_user
    end
  end

  private

  def post_params # allows certain data to be passed via form.
    params.require(:post).permit(:user_id, :content)
  end

  def update_post_params
    params.require(:post).permit(:content)
  end

  def set_post
    @post = Post.find(params[:id])
  end
end