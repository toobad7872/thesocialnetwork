class RelationshipsController < ApplicationController

  before_filter :authenticate_user!

  def create
    user = User.find(params[:followed_id])
    current_user.follow(user)
    redirect_to(:back)
  end

  def destroy
    user = Relationship.find(params[:id]).followed
    current_user.unfollow(user)
    redirect_to(:back)
  end

  def accept
    relationship = Relationship.find(params[:id])
    relationship.update(status: 'accepted')
    Notification.create(notify_type: 'follow', actor: relationship.follower, user: relationship.followed)
    redirect_to(:back)
  end

  def reject
    relationship = Relationship.find(params[:id])
    relationship.destroy
    redirect_to(:back)
  end

  def add_to_circle
    user = User.find(params[:user_id])
    relationship = current_user.active_relationships.where(followed_id: user.id).last
    if relationship.update(circle: params[:circle])
      flash[:success] = "Add User to circle"
    else
      flash[:error] = "Not able to add user to circle"
    end
    redirect_to :back
  end
end