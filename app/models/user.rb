class User < ActiveRecord::Base

  attr_accessor :login

  serialize :blocked_user_ids, Array

  # searchkick word_start: [:name, :username, :email, :bio]
  acts_as_voter
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :posts, dependent: :destroy # remove a user's posts if his account is deleted.
  has_many :comments, dependent: :destroy
  has_many :comment_giphies, dependent: :destroy
  has_many :active_relationships, -> { where(status: 'accepted') }, class_name: "Relationship", foreign_key: "follower_id", dependent: :destroy
  has_many :passive_relationships, -> { where(status: 'accepted') }, class_name: "Relationship", foreign_key: "followed_id", dependent: :destroy
  has_many :pending_relationships, -> { where(status: 'pending') }, class_name: "Relationship", foreign_key: "followed_id", dependent: :destroy
  has_many :following, through: :active_relationships, source: :followed
  has_many :followers, through: :passive_relationships, source: :follower
  has_many :pending_following, through: :pending_relationships, source: :followed
  has_many :pending_followers, through: :pending_relationships, source: :follower
  has_many :followings_users, -> { where(followable_type: 'User') }, class_name: "Follow", foreign_key: :user_id, dependent: :destroy
  has_many :reports
  has_many :reported_posts, through: :reports, source: :post
  has_many :my_followers, class_name: "Follow", foreign_key: :followable_id, dependent: :destroy
  mount_uploader :avatar, AvatarUploader
  mount_uploader :cover, CoverUploader
  has_many :conversations, :foreign_key => :sender_id
  has_many :social_net_worths,:dependent=>:destroy

  has_many :earned_point_histories, class_name: 'PointHistory', foreign_key: :user_id
  has_many :given_point_histories, class_name: 'PointHistory', foreign_key: :actor_id

  has_many :badge_users
  has_many :badges, through: :badge_users

  has_many :tokens

  belongs_to :badge

  include SimpleHashtag::Hashtaggable
  hashtaggable_attribute :bio

  default_scope { where(disable: false) }
  before_update :disable_my_data
  after_update :accept_all_requests

  scope :ignore_blocked_users, -> (user){ where.not(id: user.blocked_user_ids) }

  scope :filter_circle, -> (circle) do
    joins(:active_relationships).where("relationships.circle = ?", circle) if (circle.present? && ['Fans', 'Family'].include?(circle))
  end

  validates :dob, presence: true
  validates :username, presence: :true, uniqueness: { case_sensitive: false }
  validate :dob_grater_then_18?

  def dob_grater_then_18?
    if self.dob?
      unless self.dob.to_date < (Date.today - 18.years)
        errors.add :base, "User age should be atleast 18 years old"
      end
    end
  end

  def disable_my_data
    if disable_changed? && self.disable?
      posts.update_all(disable: true)
      comments.update_all(disable: true)
      votes.update_all(disable: true)
      UserMailer.notify_user_for_disable_account(id).deliver_now
    end
  end

  # follow another user
  def follow(other)
    if other.private
      Relationship.create(followed_id: other.id, follower_id: id)
    else
      active_relationships.create(followed_id: other.id, status: 'accepted')
      Notification.create(notify_type: 'follow', actor: self, user: other)
    end
  end

  # unfollow a user
  def unfollow(other)
    active_relationships.find_by(followed_id: other.id).destroy
  end

  # is following a user?
  def following?(other)
    following.include?(other)
  end

  def friends
    following.ignore_blocked_users(self) + followers.ignore_blocked_users(self)
  end

  def suggested_friends
    friends.map do |friend|
      friend.friends
    end.flatten.compact.uniq - (friends + [self])
  end

  def regenerate_password
    r = [ ]
    while r.length < 6
      v = rand(7)
      r << v unless r.include? v
    end
    random = r.join(",").gsub(",","")
    self.update_attributes(:password=>random, :password_confirmation => random)
    random
  end

  def blocked?(user_id)
    blocked_user_ids.include?(user_id)
  end

  def show_message_box?(user)
    return true if following?(user)
    return user.direct_message
  end

  def find_show_message_box?(conversation)
    if id != conversation.sender_id
      show_message_box?(conversation.sender)
    else
      show_message_box?(conversation.recipient)
    end
  end

  def reset_token
    token = tokens.create
    token.authentication_token
  end

  private
    def accept_all_requests
      if private_changed? && private == false
        Relationship.where(followed_id: id, status: 'pending').update_all(status: 'accepted')
      end
    end

    def self.find_for_database_authentication(warden_conditions)
      conditions = warden_conditions.dup
      if login = conditions.delete(:login)
        where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
      elsif conditions.has_key?(:username) || conditions.has_key?(:email)
        where(conditions.to_h).first
      end
    end
end