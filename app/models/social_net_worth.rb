class SocialNetWorth < ActiveRecord::Base
  belongs_to :user
  belongs_to :post
  belongs_to :pointable, :polymorphic => true
end
