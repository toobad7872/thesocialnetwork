class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new # guest user (not logged in)
    if user.admin?
      can :manage, :all
    else
      can :read, :all
      can :update, Post do |post|
        post.user_id == user.id
      end
      can :destroy, Post do |post|
        post.user_id == user.id
      end
      can :update, Comment do |comment|
        comment.user_id == user.id
      end
      can :destroy, Comment do |comment|
        comment.user_id == user.id || comment.post.user_id == user.id
      end
      can :destroy, CommentGiphy do |comment_giphy|
        comment_giphy.user_id == user.id || comment_giphy.post.user_id == user.id
      end
    end

  end
end
