class Badge < ActiveRecord::Base
  has_many :badge_users
  has_many :badges, through: :badge_users
end
