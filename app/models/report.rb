class Report < ActiveRecord::Base
  validates :user_id, presence: true
  validates :post_id, presence: true
  validates :user_id, uniqueness: {scope: :post_id}

  belongs_to :user
  belongs_to :post

  before_update :flag_post
  after_create :check_for_auto_flag, :notiify_admin

  def flag_post
    if valid_changed? && self.valid?
      post.spam_me
    end
  end

  def check_for_auto_flag
    if Report.where(post_id: post_id).count >= 10
      post.spam_me
    end
  end

  def notiify_admin
    UserMailer.notify_admin_for_spam(post_id).deliver_now
    post.create_activity key: 'posts.reported_spam', owner: user, recipient: post.user
  end
end
