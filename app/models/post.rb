class Post < ActiveRecord::Base
  # searchkick

  include PublicActivity::Model

  tracked owner: ->(controller, model) { controller && controller.current_user }
  tracked recipient: ->(controller, model) { model && model.user }

  acts_as_votable

  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :comment_giphies, dependent: :destroy
  has_many :post_uploads, dependent: :destroy
  accepts_nested_attributes_for :post_uploads

  has_many :likes, as: :likeable, dependent: :destroy
  has_many :reports
  has_many :reported_users, through: :reports, source: :user
  has_many :point_histories, as: :pointable

  validates :user_id, presence: true
  validates :content, presence: false
  default_scope -> { order(created_at: :desc) } #newest raves first
  include SimpleHashtag::Hashtaggable
  hashtaggable_attribute :content

  default_scope { where(spam: false, disable: false) }

  after_create :create_activity_for_tagging, :add_points
  after_destroy :remove_activity_for_tagging, :remove_points

  def spam_me
    update(spam: true)
    create_activity key: 'posts.marked_spam', owner: User.where(admin: true).first, recipient: user
  end

  def create_activity_for_tagging
    if content?
      users = []
      usernames = content.gsub(/<\/?[^>]*>/, '').gsub("&nbsp;", "").split(" ").map { |name| name[1..name.length] if name.start_with?("@") }.compact
      users = User.where(username: usernames) if usernames.present?
      users.each do |tagged_user|
        create_activity key: 'posts.tagged_user', owner: user, recipient: tagged_user
      end
    end
  end

  def remove_activity_for_tagging
    PublicActivity::Activity.where(trackable: self).destroy_all
  end

  def add_points
    point_histories.build(
      points: POST_POINTS.to_f,
      actor_id: user_id,
      user_id: user_id,
      action: "create.#{self.class.name.downcase}"
    ).save
  end

  def remove_points
    all_pointables = self.comments + self.comment_giphies + [self] + self.votes_for
    point_histories.where(pointable: all_pointables).destroy_all
  end
end
