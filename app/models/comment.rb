class Comment < ActiveRecord::Base
	include PublicActivity::Model

  tracked owner: ->(controller, model) { controller && controller.current_user }
  tracked recipient: ->(controller, model) { model && model.post.user }

  acts_as_votable

  belongs_to :user
  belongs_to :post
  has_many :social_net_worths,:as=>:pointable
  has_many :point_histories, as: :pointable

  validates :content, presence: true

  default_scope { where(disable: false) }

  after_create :create_activity_for_tagging, :add_points
  after_destroy :remove_activity_for_tagging, :remove_points

  def create_activity_for_tagging
    if content?
      users = []
      usernames = content.gsub(/<\/?[^>]*>/, '').gsub("&nbsp;", "").split(" ").map { |name| name[1..name.length] if name.start_with?("@") }.compact
      users = User.where(username: usernames) if usernames.present?
      users.each do |tagged_user|
        create_activity key: 'comments.tagged_user', owner: user, recipient: tagged_user
      end
    end
  end

  def remove_activity_for_tagging
    PublicActivity::Activity.where(trackable: self).destroy_all
  end

  def add_points
    if user_id != post.user_id
      user_comments = post.comments.where(user_id: user_id)
      point_history = PointHistory.where(pointable: user_comments).last
      if point_history.nil? || (Time.now - 10.minutes) > point_history.created_at
        point_histories.build(
          points: COMMENT_POINTS.to_f,
          actor_id: user_id,
          user_id: post.user_id,
          action: "create.#{self.class.name.downcase}"
        ).save
        point_histories.build(
          points: COMMENT_POINTS.to_f,
          actor_id: user_id,
          user_id: user_id,
          action: "create.#{self.class.name.downcase}"
        ).save
      end
    end
  end

  def remove_points
    if user_id != post.user_id
      point_histories.where(pointable: self).destroy_all
    end
  end
end

class ActsAsVotable::Vote
  include PublicActivity::Model
  default_scope { where(disable: false) }

  tracked owner: ->(controller, model) { controller && controller.current_user }
  tracked recipient: ->(controller, model) { model && model.votable.user }
  has_many :point_histories, as: :pointable

  after_create :add_points
  after_destroy :remove_points

  def add_points
    if voter_id != votable.user_id
      user_votes = votable.votes_for.where(voter_id: voter_id, voter_type: "User")
      point_history = PointHistory.where(pointable: user_votes).last
      if point_history.nil? || (Time.now - 10.minutes) > point_history.created_at
        point_histories.build(
          points: LIKE_POINTS.to_f,
          actor_id: voter_id,
          user_id: votable.user_id,
          action: "create.#{self.class.name.downcase}"
        ).save
        point_histories.build(
          points: LIKE_POINTS.to_f,
          actor_id: voter_id,
          user_id: voter_id,
          action: "create.#{self.class.name.downcase}"
        ).save
      end
    end
  end

  def remove_points
    if voter_id != votable.user_id
      point_histories.where(pointable: self).destroy_all
    end
  end
end