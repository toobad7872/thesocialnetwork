class CommentGiphy < ActiveRecord::Base

  belongs_to :user
  belongs_to :post
  has_many :point_histories, as: :pointable

  validates_presence_of :url, :user_id, :post_id

  after_create :add_points
  after_destroy :remove_points

  def add_points
    if user_id != post.user_id
      user_comment_giphies = post.comment_giphies.where(user_id: user_id)
      point_history = PointHistory.where(pointable: user_comment_giphies).last
      if point_history.nil? || (Time.now - 10.minutes) > point_history.created_at
        point_histories.build(
          points: COMMENT_GIPHY_POINTS.to_f,
          actor_id: user_id,
          user_id: post.user_id,
          action: "create.#{self.class.name.downcase}"
        ).save
        point_histories.build(
          points: COMMENT_GIPHY_POINTS.to_f,
          actor_id: user_id,
          user_id: user_id,
          action: "create.#{self.class.name.downcase}"
        ).save
      end
    end
  end

  def remove_points
    if user_id != post.user_id
      point_histories.where(pointable: self).destroy_all
    end
  end

end
