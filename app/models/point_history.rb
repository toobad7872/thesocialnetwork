class PointHistory < ActiveRecord::Base
  belongs_to :user
  belongs_to :actor, class_name: 'User'
  belongs_to :pointable, polymorphic: true

  validates_presence_of :points, :user_id, :actor_id, :pointable_id, :pointable_type, :action

  after_create :add_badge
  after_destroy :add_badge

  def add_badge
    last_month_points = PointHistory.where("user_id = (?) AND created_at >= ?", user_id, 30.days).pluck(:points).inject(:+)
    badge_name = ""
    if last_month_points > BEGINNER_LEVEL && last_month_points < GO_GETTER_LEVEL-1
      badge_name = "Beginner Level"
    elsif last_month_points > GO_GETTER_LEVEL && last_month_points < BOSS_LEVEL-1
      badge_name = "Go Getter Level"
    elsif last_month_points > BOSS_LEVEL && last_month_points < STAR_LEVEL-1
      badge_name = "Boss Level"
    elsif last_month_points > STAR_LEVEL && last_month_points < LEGEND_LEVEL-1
      badge_name = "Star Level"
    elsif last_month_points > LEGEND_LEVEL
      badge_name = "Legend Level"
    elsif last_month_points < BEGINNER_LEVEL-1
      user.update(badge_id: nil)
    end
    if badge_name.present? && user.badge.try(:name) != badge_name
      badge = Badge.where(name: badge_name).first
      if badge.present?
        user.badges << badge
        user.update(badge_id: badge.id)
      end
    end
  end
end
