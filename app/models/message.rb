class Message < ActiveRecord::Base
  belongs_to :conversation
  belongs_to :user

  validates_presence_of :conversation_id, :user_id

  mount_uploader :attachment, AttachmentUploader

  validate :attachment_or_gif_or_body_exist?

  def attachment_or_gif_or_body_exist?
    unless self.body? || self.attachment? || self.gif?
      errors.add :base, "Either Body Or Attach Or Gif should present."
    end
  end

end
