var all_validations = function(){
	$('#login').validate({
	  rules: {
	    "user[email]": {
		    required: true
	    },
	    "user[password]": {
		    required: true,
	    }
	  },
	  messages: {
	  	"user[email]": {
	      required: "The email is required"
	    },
	    "user[password]":{
        required: 'The password is required'
	    }
	  }
	})

	$('#signup').validate({
	  rules: {
	    "user[username]": {
		    required: true
	    },
	    "user[email]": {
		    required: true,
	      email: true
	    },
	    "user[password]": {
		    required: true,
		    minlength: 6
	    },
	    "user[password_confirmation]": {
		    required: true,
		    minlength: 6
	    }
	  },
	  messages: {
	  	"user[username]": {
	      required: "The username is required",
	    },
	  	"user[email]": {
	      required: "The email is required",
	      email: "Your email address must be in the format of name@domain.com"
	    },
	    "user[password]":{
        minlength: "Your password must be at least 6 characters long",
        required: 'The password is required'
	    },
	    "user[password_confirmation]":{
        minlength: "Your password must be at least 6 characters long",
        required: 'The password confirmation is required'
	    }
	  }
	})


	$('.hide-chat').click(function(){
		$('.chat_block').hide();
		$('.show_button').show();
		$('.chat-sidebar').css({height: 'auto'})
	})

	$('.show-chat').click(function(){
		$('.chat_block').show();
		$('.show_button').hide();
		$('.chat-sidebar').css({height: '300px'})
	})

};


$(document).ready(all_validations);
$(document).on("page:load", all_validations);