class UserMailer < ApplicationMailer

  def notify_admin_for_spam(post_id)
    @post = Post.where(id: post_id).last
    @users = User.where(admin: true)
    @users.each do |user|
      @user = user
      mail(to: @user.email, subject: 'A Post is reported spam by a user')
    end
  end

  def notify_user_for_disable_account(user_id)
    @user = User.unscoped.where(id: user_id).last
    mail(to: @user.email, subject: 'You account is disabled')
  end
  

  def reset_password_link(current_user)
    @token = current_user.authentication_token
    @resource = current_user
    mail(to: current_user[:email], subject: 'Reset password Link')
  end
end
