module ConvertTimeZone
  def created_at_convert_timezone(timezone = nil)
    timezone ? created_at.in_time_zone(timezone) : created_at
  end

  def updated_at_convert_timezone(timezone = nil)
    timezone ? updated_at.in_time_zone(timezone) : updated_at
  end
end
