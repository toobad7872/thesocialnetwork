# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190521174040) do

  create_table "activities", force: :cascade do |t|
    t.integer  "trackable_id",   limit: 4
    t.string   "trackable_type", limit: 255
    t.integer  "owner_id",       limit: 4
    t.string   "owner_type",     limit: 255
    t.string   "key",            limit: 255
    t.text     "parameters",     limit: 65535
    t.integer  "recipient_id",   limit: 4
    t.string   "recipient_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "badge_users", force: :cascade do |t|
    t.integer  "badge_id",   limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "badges", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "comment_giphies", force: :cascade do |t|
    t.string   "url",        limit: 255
    t.integer  "user_id",    limit: 4
    t.integer  "post_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "comment_giphies", ["post_id"], name: "index_comment_giphies_on_post_id", using: :btree
  add_index "comment_giphies", ["user_id"], name: "index_comment_giphies_on_user_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "post_id",    limit: 4
    t.text     "content",    limit: 65535
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.boolean  "disable",                  default: false
  end

  add_index "comments", ["post_id"], name: "index_comments_on_post_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "conversations", force: :cascade do |t|
    t.integer  "sender_id",    limit: 4
    t.integer  "recipient_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "conversations", ["recipient_id"], name: "index_conversations_on_recipient_id", using: :btree
  add_index "conversations", ["sender_id"], name: "index_conversations_on_sender_id", using: :btree

  create_table "follows", force: :cascade do |t|
    t.string   "followable_type", limit: 255
    t.integer  "followable_id",   limit: 4
    t.integer  "user_id",         limit: 4
    t.boolean  "is_active",                   default: true
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "likes", force: :cascade do |t|
    t.string   "likeable_type", limit: 255
    t.integer  "likeable_id",   limit: 4
    t.integer  "user_id",       limit: 4
    t.boolean  "is_active",                 default: true
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "messages", force: :cascade do |t|
    t.text     "body",            limit: 65535
    t.integer  "conversation_id", limit: 4
    t.integer  "user_id",         limit: 4
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.string   "attachment",      limit: 255
    t.string   "gif",             limit: 255
    t.boolean  "read",                          default: false, null: false
  end

  add_index "messages", ["conversation_id"], name: "index_messages_on_conversation_id", using: :btree
  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id",            limit: 4,   null: false
    t.integer  "actor_id",           limit: 4
    t.string   "notify_type",        limit: 255, null: false
    t.string   "target_type",        limit: 255
    t.integer  "target_id",          limit: 4
    t.string   "second_target_type", limit: 255
    t.integer  "second_target_id",   limit: 4
    t.string   "third_target_type",  limit: 255
    t.integer  "third_target_id",    limit: 4
    t.datetime "read_at"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "notifications", ["user_id", "notify_type"], name: "index_notifications_on_user_id_and_notify_type", using: :btree
  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "point_histories", force: :cascade do |t|
    t.float    "points",         limit: 24
    t.integer  "user_id",        limit: 4
    t.string   "action",         limit: 255
    t.integer  "actor_id",       limit: 4
    t.integer  "pointable_id",   limit: 4
    t.string   "pointable_type", limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "post_uploads", force: :cascade do |t|
    t.integer  "post_id",    limit: 4
    t.string   "video",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "post_uploads", ["post_id"], name: "index_post_uploads_on_post_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.text     "content",    limit: 65535
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.string   "video",      limit: 255
    t.text     "body",       limit: 65535
    t.boolean  "spam",                     default: false
    t.boolean  "disable",                  default: false
    t.string   "circle",     limit: 255,   default: "Everyone", null: false
  end

  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "relationships", force: :cascade do |t|
    t.integer  "follower_id", limit: 4
    t.integer  "followed_id", limit: 4
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "status",      limit: 255, default: "pending"
    t.string   "circle",      limit: 255, default: "Friend",  null: false
  end

  add_index "relationships", ["followed_id"], name: "index_relationships_on_followed_id", using: :btree
  add_index "relationships", ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true, using: :btree
  add_index "relationships", ["follower_id"], name: "index_relationships_on_follower_id", using: :btree

  create_table "reports", force: :cascade do |t|
    t.integer  "post_id",     limit: 4
    t.integer  "user_id",     limit: 4
    t.text     "description", limit: 65535
    t.boolean  "valid",                     default: false
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "reports", ["user_id", "post_id"], name: "index_reports_on_user_id_and_post_id", unique: true, using: :btree

  create_table "simple_hashtag_hashtaggings", force: :cascade do |t|
    t.integer "hashtag_id",        limit: 4
    t.integer "hashtaggable_id",   limit: 4
    t.string  "hashtaggable_type", limit: 255
  end

  add_index "simple_hashtag_hashtaggings", ["hashtag_id"], name: "index_simple_hashtag_hashtaggings_on_hashtag_id", using: :btree
  add_index "simple_hashtag_hashtaggings", ["hashtaggable_id", "hashtaggable_type"], name: "index_hashtaggings_hashtaggable_id_hashtaggable_type", using: :btree

  create_table "simple_hashtag_hashtags", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "simple_hashtag_hashtags", ["name"], name: "index_simple_hashtag_hashtags_on_name", using: :btree

  create_table "social_net_worths", force: :cascade do |t|
    t.float    "point",          limit: 24
    t.float    "amount",         limit: 24
    t.integer  "user_id",        limit: 4
    t.integer  "post_id",        limit: 4
    t.boolean  "redeem_status",              default: false
    t.integer  "pointable_id",   limit: 4
    t.string   "pointable_type", limit: 255
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "social_net_worths", ["post_id"], name: "index_social_net_worths_on_post_id", using: :btree
  add_index "social_net_worths", ["user_id"], name: "index_social_net_worths_on_user_id", using: :btree

  create_table "tokens", force: :cascade do |t|
    t.string   "authentication_token", limit: 255
    t.integer  "user_id",              limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "tokens", ["user_id"], name: "index_tokens_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255,   default: "",         null: false
    t.string   "encrypted_password",     limit: 255,   default: "",         null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,          null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.string   "username",               limit: 255
    t.string   "avatar",                 limit: 255
    t.string   "cover",                  limit: 255
    t.text     "bio",                    limit: 65535
    t.boolean  "admin",                                default: false
    t.boolean  "disable",                              default: false
    t.string   "blocked_user_ids",       limit: 255,   default: "--- []\n"
    t.boolean  "private",                              default: false
    t.string   "authentication_token",   limit: 255
    t.string   "age",                    limit: 255
    t.boolean  "direct_message",                       default: true
    t.string   "timezone",               limit: 255
    t.string   "gender",                 limit: 255
    t.date     "dob"
    t.string   "name",                   limit: 255
    t.text     "tagline",                limit: 65535
    t.integer  "badge_id",               limit: 4
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "votes", force: :cascade do |t|
    t.integer  "votable_id",   limit: 4
    t.string   "votable_type", limit: 255
    t.integer  "voter_id",     limit: 4
    t.string   "voter_type",   limit: 255
    t.boolean  "vote_flag"
    t.string   "vote_scope",   limit: 255
    t.integer  "vote_weight",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "disable",                  default: false
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

  add_foreign_key "comment_giphies", "posts"
  add_foreign_key "comment_giphies", "users"
  add_foreign_key "comments", "posts"
  add_foreign_key "comments", "users"
  add_foreign_key "messages", "conversations"
  add_foreign_key "messages", "users"
  add_foreign_key "post_uploads", "posts"
  add_foreign_key "posts", "users"
  add_foreign_key "social_net_worths", "posts"
  add_foreign_key "social_net_worths", "users"
  add_foreign_key "tokens", "users"
end
