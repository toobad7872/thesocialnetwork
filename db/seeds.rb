# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


if User.count == 0
  User.create(email: "Ljordan869@gmail.com", username: "Larry", admin: true, password: 'password1', password_confirmation: 'password1', dob: '1980-09-12', timezone: "American Samoa")
end


if Badge.count == 0
  badges = [
    {name: "Beginner Level", description: "You can post 3 photos per posts and can post 30 sec video post"},
    {name: "Go Getter Level", description: "You can post 8 photos per posts and can post 1 min video post"},
    {name: "Boss Level", description: "You can post 12 photos per posts and can post 2 min video post"},
    {name: "Star Level", description: "You can post 15 photos per posts and can post 5 min video post"},
    {name: "Legend Level", description: "You can post 15+ photos per posts and can post 5 min+ video post"}]
  badges.each {|badge| Badge.create(badge) }
end