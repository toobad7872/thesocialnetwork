class AddDisableColumnToRelatedTables < ActiveRecord::Migration
  def up
    add_column :users, :disable, :boolean, default: false
    add_column :comments, :disable, :boolean, default: false
    add_column :votes, :disable, :boolean, default: false
    add_column :posts, :disable, :boolean, default: false
  end

  def down
    remove_column :users, :disable
    remove_column :comments, :disable
    remove_column :votes, :disable
    remove_column :posts, :disable
  end
end
