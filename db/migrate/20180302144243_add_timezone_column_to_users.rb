class AddTimezoneColumnToUsers < ActiveRecord::Migration
  def up
    add_column :users, :timezone, :string
  end

  def down
    remove_column :users, :timezone
  end
end
