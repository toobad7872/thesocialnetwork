class AddCircleColumnToRelationshipsTable < ActiveRecord::Migration
  def up
    add_column :relationships, :circle, :string, default: 'Everyone', null: false
  end

  def down
    remove_column :relationships, :circle, :string
  end
end
