class AddStatusToRelationships < ActiveRecord::Migration
  def up
    add_column :relationships, :status, :string, default: 'pending'
    Relationship.update_all(status: 'accepted')
  end

  def down
    remove_column :relationships, :status
  end
end
