class AddAttachmentToMessages < ActiveRecord::Migration
  def up
    add_column :messages, :attachment, :string
  end

  def down
    remove_column :messages, :attachment
  end
end
