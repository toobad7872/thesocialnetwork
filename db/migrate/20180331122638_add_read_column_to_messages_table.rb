class AddReadColumnToMessagesTable < ActiveRecord::Migration
  def up
    add_column :messages, :read, :boolean, default: false, null: false
  end

  def down
    remove_column :messages, :read
  end
end
