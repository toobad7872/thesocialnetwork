class AddBadgeIdToUsers < ActiveRecord::Migration
  def up
    add_column :users, :badge_id, :integer
  end

  def down
    remove_column :users, :badge_id
  end
end
