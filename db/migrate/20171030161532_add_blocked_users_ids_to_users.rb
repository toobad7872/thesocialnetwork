class AddBlockedUsersIdsToUsers < ActiveRecord::Migration
  def up
    add_column :users, :blocked_user_ids, :string, array: true, default: []
  end

  def down
    remove_column :users, :blocked_user_ids, :string
  end
end
