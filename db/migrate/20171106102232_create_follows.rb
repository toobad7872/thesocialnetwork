class CreateFollows < ActiveRecord::Migration
  def change
    create_table :follows do |t|
      t.string :followable_type
      t.integer :followable_id
      t.integer :user_id
      t.boolean :is_active, default: true

      t.timestamps null: false
    end
  end
end
