class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :post_id
      t.integer :user_id
      t.text :description
      t.boolean :valid, default: false
      t.timestamps null: false
    end
    add_index :reports, [:user_id, :post_id], unique: true
  end
end
