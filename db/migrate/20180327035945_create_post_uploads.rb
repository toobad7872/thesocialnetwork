class CreatePostUploads < ActiveRecord::Migration
  def change
    create_table :post_uploads do |t|
      t.references :post, index: true, foreign_key: true
      t.string     :video
      t.timestamps null: false
    end
  end
end
