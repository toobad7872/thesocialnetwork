class AddCircleToPosts < ActiveRecord::Migration
  def up
    add_column :posts, :circle, :string, default: 'Everyone', null: false
  end

  def down
    remove_column :posts, :circle, :string
  end
end
