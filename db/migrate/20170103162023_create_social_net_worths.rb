class CreateSocialNetWorths < ActiveRecord::Migration
  def change
    create_table :social_net_worths do |t|
      t.float :point
      t.float :amount
      t.references :user, index: true, foreign_key: true
      t.references :post, index: true, foreign_key: true
      t.boolean :redeem_status,:default=>false
      t.integer :pointable_id
      t.string :pointable_type

      t.timestamps null: false
    end
  end
end
