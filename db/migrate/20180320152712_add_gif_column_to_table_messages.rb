class AddGifColumnToTableMessages < ActiveRecord::Migration
  def up
    add_column :messages, :gif, :string
  end

  def down
    remove_column :messages, :gif
  end
end
