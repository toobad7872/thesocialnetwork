class AddSpamColumnToPosts < ActiveRecord::Migration
  def up
    add_column :posts, :spam, :boolean, default: false
  end

  def down
    remove_column :posts, :spam
  end
end
