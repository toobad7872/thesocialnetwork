class CreateCommentGiphies < ActiveRecord::Migration
  def change
    create_table :comment_giphies do |t|
      t.string :url
      t.references :user, index: true, foreign_key: true
      t.references :post, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
