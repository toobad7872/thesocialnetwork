class AddColumnPrivateToUsers < ActiveRecord::Migration
  def up
    add_column :users, :private, :boolean, default: false
  end

  def down
    remove_column :users, :private, :boolean
  end
end
