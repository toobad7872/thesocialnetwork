class AddDirectMessageColumnToUsersTable < ActiveRecord::Migration
  def up
    add_column :users, :direct_message, :boolean, default: true
  end

  def down
    add_column :users, :direct_message
  end
end
