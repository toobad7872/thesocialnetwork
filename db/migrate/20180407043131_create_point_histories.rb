class CreatePointHistories < ActiveRecord::Migration
  def change
    create_table :point_histories do |t|
      t.float      :points
      t.integer    :user_id
      t.string     :action
      t.integer    :actor_id
      t.integer    :pointable_id
      t.string     :pointable_type
      t.timestamps null: false
    end
  end
end
