class AddGenderDobAndNameToUsersTable < ActiveRecord::Migration
  def up
    add_column :users, :gender, :string
    add_column :users, :dob, :date
    add_column :users, :name, :string
  end

  def down
    remove_column :users, :gender
    remove_column :users, :dob
    remove_column :users, :name
  end
end
